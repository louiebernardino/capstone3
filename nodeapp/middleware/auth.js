const jwt = require("jsonwebtoken")
const User = require("../models/users")

const auth = async (req, res, next) => {
	try {
		//1 access header
		//2 get jwt out of the header
		const token = req.header("Authorization").replace("Bearer ", "")
		//3 make sure that token is valid
		const decoded = jwt.verify(token, "batch40")
		//4 find the member in db
		//5 check if token is part of member.tokens
		const user = await User.findOne({_id: decoded._id, "tokens.token": token})
		//6 check if member doesn't exist
		if(!user) {
			throw new Error("User doesn't exist!")
		}
		//7 give route access to member
		req.user = user
		//8 give route access to token
		req.token = token
		//9 call next function
		next()

	} catch(e) {
		//unauth
		res.status(401).send({ "message": "Please Authenticate!"})
	}
}

module.exports = auth