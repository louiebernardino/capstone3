//Declare dependencies
const mongoose = require("mongoose");
const Schema = mongoose.Schema;
const validator = require("validator")
const Float = require("mongoose-float").loadType(mongoose)
const jwt = require("jsonwebtoken")
const bcrypt = require('bcryptjs')

//Define your schema
const barberSchema = new Schema(
	{
		firstName: {
			type: String,
			trim: true,
			maxlength: 30,
			default: ""
		},
		lastName: {
			type: String,
			trim: true,
			maxlength: 30,
			default: ""
		},
		isAvailable: {
			type: Boolean,
			default: true
		},
		price: {
			type: Float,
			required: true
		},
		userName: {
			type: String,
			required: true,
			trim: true,
			maxlength: 30,
			index: true,
			unique: true,
			validate(value) {
				if(!validator.isAlphanumeric(value)) {
					throw new Error("No special characters!")
				}
				console.log("Username for barber has been added")
			}
		},
		email: {
			type: String,
			required: true,
			trim: true,
			unique: true,
			validate(value) {
				if(!validator.isEmail(value)) {
					throw new Error("Email is invalid!")
				}
			}
		},
		password: {
			type: String,
			required: true,
			minlength: [5, "Password must have at least 5 characters!"]
		},
		position: {
			type: String,
			default: "barber"
		},
		barberPhoto: {
			type: Buffer,
			default: undefined
		},
		tokens: [
			{
				token: {
					type: String,
					required: true
				}
			}
		]
	},
	{
		timestamps: true
	}
)

//Generate Token
barberSchema.methods.generateAuthToken = async function() {
	const barber = this

	//1 - data to embed
	//2 - secret message
	//3 - option
	const token = jwt.sign({_id: barber._id.toString()}, "barbersTok", {expiresIn: "2 days"})
	barber.tokens = barber.tokens.concat({token})
	await barber.save()
	return token

	//save the token to our db
}

//SET THE RELATION BETWEEN BARBERS AND USERS
barberSchema.virtual("users", {
	ref: "User",
	localField: "_id", //team._id
	foreignField: "barberId"
})

//HASH PASSWORD WHEN UPDATING AND CREATING A BARBER
barberSchema.pre("save", async function(next) { 
	//we will use function because arrow function ()=> does not bind this
	const barber = this

	if(barber.isModified('password')) {
		//salt
		const salt = await bcrypt.genSalt(10);
		//hash
		barber.password = await bcrypt.hash(barber.password, salt);
	}

	next();
})

//Export your model
module.exports = mongoose.model("Barber", barberSchema);