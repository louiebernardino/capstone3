const mongoose = require("mongoose");
const Schema = mongoose.Schema;
const uniqueValidator = require("mongoose-unique-validator");
const validator = require("validator");
const bcrypt = require('bcryptjs')
const jwt = require("jsonwebtoken")

//Define your schema
const userSchema = new Schema(
	{
		firstName: {
			type: String,
			trim: true,
			maxlength: 30,
			default: ""
		},

		lastName: {
			type: String,
			trim: true,
			maxlength: 30,
			default: ""
		},
		userName: {
			type: String,
			required: true,
			trim: true,
			maxlength: 30,
			index: true,
			unique: true,
			validate(value) {
				if(!validator.isAlphanumeric(value)) {
					throw new Error("No special characters!")
				}
				console.log("Username for user has been added")
			}
		},
		position: {
			type: String,
			enum: ["admin", "a bro"],
			required: false
		},
		age: {
			type: Number,
			min: 18,
			required: false
		},
		email:
		{
			type: String,
			required: true,
			trim: true,
			unique: true,
			validate(value) {
				if(!validator.isEmail(value)) {
					throw new Error("Email is invalid!")
				}
			}
		},
		password:
		{
			type: String,
			required: true,
			minlength: [5, "Password must have at least 5 characters!"]
		},
		//THE CONNECTION BETWEEN BARBERS AND USERS
		barberId: {
			type: mongoose.Schema.Types.ObjectId,
			// required: true,
			ref: "Barber"
		},
		validIdPic: {
			type: Buffer,
			default: undefined
		},
		tokens: [
			{
				token: {
					type: String,
					required: true
				}
			}
		]
		
	},
	{
		timestamps: true
	}

);

//HIDE PRIVATE DATA
userSchema.methods.toJSON = function() {
	const user = this

	//return RAW member object with ALL its fields
	const userObj = user.toObject()

	//delete a private data
	delete userObj.password

	//delete all tokens
	delete userObj.tokens

	//DELETE PROFILE PIC
	// delete userObj.profilePic

	return userObj
} 

//SET THE RELATION BETWEEN AND USERS AND RESERVATIONS
userSchema.virtual("reservations", {
	ref: "Reservation",
	localField: "_id", //user._id
	foreignField: "userId"
})

//HASH PASSWORD WHEN UPDATING AND CREATING A USER
userSchema.pre("save", async function(next) { 
	//we will use function because arrow function ()=> does not bind this
	const user = this

	if(user.isModified('password')) {
		//salt
		const salt = await bcrypt.genSalt(10);
		//hash
		user.password = await bcrypt.hash(user.password, salt);
	}

	next();
})

//find by credentials using statics
// userSchema.statics.findByCredentials = async(email, password) => {
// 	//check if email exists
// 	const user = await User.findOne({ email })

// 	if(!user) {
// 		throw new Error("Email is wrong!") //for now
// 	}

// 	//comapring req pw  vs unhashed db password
// 	const isMatch = await bcrypt.compare(password, user.password)

// 	if(!isMatch) {
// 		throw new Error("Wrong Password!")
// 	}

// 	return user
// }

//Generate Token
userSchema.methods.generateAuthToken = async function() {
	const user = this

	//1 - data to embed
	//2 - secret message
	//3 - option
	const token = jwt.sign({_id: user._id.toString()}, "userTok", {expiresIn: "2 days"})
	user.tokens = user.tokens.concat({token})
	await user.save()
	return token

	//save the token to our db
}

userSchema.plugin(uniqueValidator)

//export your model
const User = mongoose.model("User", userSchema)
module.exports = User