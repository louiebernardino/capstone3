//Declare dependencies
const mongoose = require("mongoose");
const Schema = mongoose.Schema;
const Float = require("mongoose-float").loadType(mongoose)

//Define your schema
const reservationSchema = new Schema(
	{
		description: {
			type: String,
			required: true,
			maxlength: 100
		},
		userId: {
			type: mongoose.Schema.Types.ObjectId,
			required: true,
			ref: "User"

		},
		reservationPrice: {
			type: Float,
			default: 50.00
		},
		isDone: {
			type: Boolean,
			default: false
		}
	},
	{
		timestamps: true
	}
);

//Export your model
module.exports = mongoose.model("Reservation", reservationSchema);