//Declare dependencies
const express = require("express");
const app = express();
const mongoose = require("mongoose");
const multer = require("multer")
const cors = require("cors")
//----------------------------------------


//Connect to DB
mongoose.connect("mongodb://localhost:27017/capstone3", 
{useNewUrlParser: true, useUnifiedTopology: true, useFindAndModify: false, useCreateIndex: true}); //DeprecationWarning

mongoose.connection.once("open", () => {
	console.log("Now connected to local MongoDB");
});
//-----------------------------------------

//Apply Middleware (auth, validation) Request response pipeline
app.use(express.json());
app.use(express.urlencoded({extended: true}));
app.use(cors());
//-----------------------------------------

//Declare Models
const Barbers = require("./models/barbers");
const Reservation = require("./models/reservations");
const User = require("./models/users");
//-----------------------------------------

//Create Routes/Endpoints
//Transferred Routes
//Declare the resources
const barbersRoute = require("./routes/barbers")
app.use("/barbers", barbersRoute)
const reservationsRoute = require("./routes/reservations")
app.use("/reservations", reservationsRoute)
const usersRoute = require("./routes/users")
app.use("/users", usersRoute)

//Configure Multer (we can configure where the file can be saved)
// const upload = multer({
// 	dest: "images/members", // "dest:" - destination
// 	limits: {
// 		filesize: 1000000 //max file size in bytes
// 	},
// 	fileFilter(req, file, cb) {
// 		//https://regex101.com
// 		if(!file.originalname.match(/\.(jpg|jpeg|png|PNG|JPEG)$/)) {
// 			return cb(new Error("Please upload an image only!"))
// 		}

// 		cb(undefined, true)

// 	}
// })

//Sample: Endpoint to upload a file
// app.post("/upload", upload.single("secretfile"), (req,res) => {
// 	// try {
// 	// 	res.send({ message: "Successfully uploaded image!" })
// 	// } catch(e) {
// 	// 	//BAD REQUEST
// 	// 	res.status(400).send({ error: e.message })
// 	// }
// 	res.send({ message: "Successfully uploaded image!" })
// }, (error, req, res, next) => {
// 	res.status(400).send({error: message})
// })  // upload a file

//Initialize the server
app.listen(4060, () => {
	console.log("Now listening to port 4060 :)");
});

