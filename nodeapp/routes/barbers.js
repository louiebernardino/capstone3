const Barber = require("../models/barbers")
const express = require("express")
const router = express.Router() //to handle routing
const auth = require("../middleware/auth")
const multer = require("multer")
const sharp = require("sharp")
const bcrypt = require("bcryptjs")



//ADD A BARBER 
router.post("/", async (req, res) => {
	// console.log("test");
	// return res.send(req.body);
	const barber = new Barber(req.body);
	try {
		await barber.save()
		res.send(barber)
	} catch(e) {
		res.status(400).send(e)
	}
})

//LOGIN A BARBER
router.post("/login2", async(req, res) => {
	try	{
		//submit email and password
		const barber = await Barber.findOne({email: req.body.email})
		if(!barber) {
			return res.send({"message": "Invalid Login Credentials!"})
		}

		const isMatch = await bcrypt.compare(req.body.password, barber.password)

		if(!isMatch) {
			return res.send({ "message": "Invalid Login Credentials!"})
		}

		//generate token
		const token = await barber.generateAuthToken()
		res.send({barber, token})
	} catch(e) {
		res.status(500).send(e)
	}
})



module.exports = router