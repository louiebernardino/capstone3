const User = require("../models/users");
const express = require("express");
const router = express.Router(); //to handle routing
const bcrypt = require("bcryptjs")
const auth = require("../middleware/auth")
const multer = require("multer")
const sharp = require("sharp")


//CREATE A USER
router.post("/", async (req, res) => {
const user = new User(req.body);
	//Commented out for async operation//
	// member.save()
	// 	.then(() => {
	// 		res.send(member)
	// 	})
	// 	.catch((e) => {
	// 		res.status(400).send(e)
	// 	})
	//Commented out for async operation//
	try {
		await user.save(); 
		res.status(201).send(user);
	} catch(e) {
		res.status(400).send(e.message);
	}
})

//LOGIN
router.post("/login", async(req, res) => {
	try	{
		//submit email and password
		const user = await User.findOne({email: req.body.email})
		if(!user) {
			return res.send({"message": "Invalid Login Credentials!"})
		}

		const isMatch = await bcrypt.compare(req.body.password, user.password)

		if(!isMatch) {
			return res.send({ "message": "Invalid Login Credentials!"})
		}

		//generate token
		const token = await user.generateAuthToken()
		res.send({user, token})
	} catch(e) {
		res.status(500).send(e)
	}
})





module.exports = router;