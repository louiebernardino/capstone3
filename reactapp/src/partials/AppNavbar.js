import React, { useState } from 'react';
import {
  Collapse,
  Navbar,
  NavbarToggler,
  NavbarBrand,
  Nav,
  NavItem,
  NavLink,
  UncontrolledDropdown,
  DropdownToggle,
  DropdownMenu,
  DropdownItem,
  NavbarText
} from 'reactstrap';
import { Button } from 'reactstrap';

const Example = (props) => {
  const [isOpen, setIsOpen] = useState(false);

  const toggle = () => setIsOpen(!isOpen);

  return (
    <div className="mb-5">
      <Navbar color="secondary" light expand="md">
        <NavbarBrand className="font-weight-bold text-white" href="/">MERN TRACKER</NavbarBrand>
        <NavbarToggler onClick={toggle} />
        <Collapse isOpen={isOpen} navbar>
          <Nav className="mr-auto" navbar>
            <NavItem className="text-black">
              <NavLink href="/components/">Members</NavLink>
            </NavItem>
            <NavItem>
              <NavLink href="https://github.com/reactstrap/reactstrap">Teams</NavLink>
            </NavItem>
            <NavItem>
              <NavLink href="https://github.com/reactstrap/reactstrap">Tasks</NavLink>
            </NavItem>
          </Nav>
          <Nav>
          <NavItem><Button color="primary" size="sm">Login</Button></NavItem>
          <NavItem><NavLink size="sm">Profile</NavLink></NavItem>
          <NavItem><Button color="secondary" size="sm">Logout</Button></NavItem>
          </Nav>
        </Collapse>
      </Navbar>
    </div>
  );
}

export default Example;